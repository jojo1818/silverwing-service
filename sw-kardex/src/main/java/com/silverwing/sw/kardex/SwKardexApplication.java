package com.silverwing.sw.kardex;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.silverwing.sw.kardex.mapper.auto")
public class SwKardexApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwKardexApplication.class, args);
    }

}
