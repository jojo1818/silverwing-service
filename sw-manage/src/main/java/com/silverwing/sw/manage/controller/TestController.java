package com.silverwing.sw.manage.controller;

import com.silverwing.sw.manage.model.entity.Test;
import com.silverwing.sw.manage.service.TestSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    TestSerivce testSerivce;

    @RequestMapping(value = "test")
    public Test getTest(){
        return testSerivce.getTest();
    }
}
