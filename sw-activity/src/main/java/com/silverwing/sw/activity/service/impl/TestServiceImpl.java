package com.silverwing.sw.activity.service.impl;

import com.silverwing.sw.activity.mapper.auto.TestMapper;
import com.silverwing.sw.activity.model.entity.Test;
import com.silverwing.sw.activity.service.TestSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("testService")
public class TestServiceImpl implements TestSerivce {

    @Autowired
    TestMapper testMapper;

    @Override
    public Test getTest() {
        return testMapper.selectByPrimaryKey(1);
    }
}
