package com.silverwing.sw.manage.service.impl;

import com.silverwing.sw.manage.mapper.auto.TestMapper;
import com.silverwing.sw.manage.model.entity.Test;
import com.silverwing.sw.manage.service.TestSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("testService")
public class TestServiceImpl implements TestSerivce {

    @Autowired
    TestMapper testMapper;

    @Override
    public Test getTest() {
        return testMapper.selectByPrimaryKey(1);
    }
}
