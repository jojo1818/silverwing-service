package com.silverwing.sw.manage;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.silverwing.sw.manage.mapper.auto")
public class SwManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwManageApplication.class, args);
    }

}
