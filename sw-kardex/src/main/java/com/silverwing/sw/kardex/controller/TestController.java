package com.silverwing.sw.kardex.controller;

import com.silverwing.sw.kardex.model.entity.Test;
import com.silverwing.sw.kardex.service.TestSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    TestSerivce testSerivce;

    @RequestMapping(value = "test")
    public Test getTest(){
        return testSerivce.getTest();
    }
}
