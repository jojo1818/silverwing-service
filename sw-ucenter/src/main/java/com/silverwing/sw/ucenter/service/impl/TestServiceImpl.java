package com.silverwing.sw.ucenter.service.impl;

import com.silverwing.sw.ucenter.mapper.auto.TestMapper;
import com.silverwing.sw.ucenter.model.entity.Test;
import com.silverwing.sw.ucenter.service.TestSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("testService")
public class TestServiceImpl implements TestSerivce {

    @Autowired
    TestMapper testMapper;

    @Override
    public Test getTest() {
        return testMapper.selectByPrimaryKey(1);
    }
}
