package com.silverwing.sw.ucenter;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
@MapperScan("com.silverwing.sw.ucenter.mapper.*")
public class SwUcenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwUcenterApplication.class, args);
    }

}
