package com.silverwing.sw.activity;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.silverwing.sw.activity.mapper.auto")
public class SwActivityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwActivityApplication.class, args);
    }

}
